const fs = require("fs/promises");
const path = require("path");

function fetchAllUsers(url) {
  return fetch(url)
    .then((response) => {
      if (response.ok) {
        return Promise.resolve(response.json());
      } else {
        return Promise.reject(`${response.status} - ${response.statusText}`);
      }
    })
    .then((data) => {
      return fs
        .writeFile(path.join(__dirname, "allusers.json"), JSON.stringify(data))
        .then(() => {
          return Promise.resolve(data);
        })
        .catch((err) => {
          return Promise.reject(err);
        });
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function fetchAllTodos(url) {
  return fetch(url)
    .then((response) => {
      if (response.ok) {
        return Promise.resolve(response.json());
      } else {
        return Promise.reject(`${response.status} - ${response.statusText}`);
      }
    })
    .then((data) => {
      return fs
        .writeFile(path.join(__dirname, "allTodos.json"), JSON.stringify(data))
        .then(() => {
          return Promise.resolve(data);
        })
        .catch((err) => {
          return Promise.reject(err);
        });
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function fetchUserAndThenTodos(usersUrl, todoUrl) {
  fetchAllUsers(usersUrl)
    .then((usersData) => {
      console.log(usersData);
      return fetchAllTodos(todoUrl);
    })
    .then((todoData) => {
      console.log(todoData);
    })
    .catch((err) => {
      console.log(err);
    });
}

function fetchUsersAndTheirDetails(usersUrl) {
  return fetchAllUsers(usersUrl).then((usersData) => {
    let usersUrlId = usersData.map((user) => {
      console.log(user.id);
      return fetchEachUser(usersUrl + "?id=" + user.id);
    });

    return Promise.allSettled(usersUrlId);
  });
}

function fetchEachUser(userUrl) {
  return fetch(userUrl)
    .then((response) => {
      if (response.ok) {
        return Promise.resolve(response.json());
      } else {
        console.log("Failed to fetch user for url " + userUrl);
        return Promise.reject(`${response.status} - ${response.statusText}`);
      }
    })
    .then((data) => {
      return fs
        .writeFile(path.join(__dirname, data[0].id + ".json"), JSON.stringify(data))
        .then(() => {
          return Promise.resolve(data);
        })
        .catch((err) => {
          return Promise.reject(err);
        });
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function fetchFirstTodoAndItsUser(todoUrl, userUrl) {
  fetchAllTodos(todoUrl)
    .then((todoData) => {
      let firstTodo = todoData[0];
      console.log(firstTodo);
      return fetchEachUser(userUrl + "?id=" + firstTodo.userId);
    })
    .then((user) => {
      console.log(user);
    })
    .catch((err) => {
      console.log(err);
    });
}
